# Rick And Morty characters application
This app has been made for the Collibra Frontend Coding Challenge in approximatively 5h.

## Install
You need to have a git client installed and a dependency manager such as yarn or npm.
* clone git repo
* install dependencies
* run start script
* if your browser doesn't automatically open, go to [localhost](http://localhost:3000)
```sh
cd <YOUR_WORKSPACE_FOLDER>
git clone https://github.com/toniocodo/rickAndMorty.git
cd rickAndMorty
yarn install
yarn start
```

## Libraries
I have used the following libraries:
* `Redux`: I use this state manager pattern as the central store management for data and ui state. The goal of splitting between `data` and `ui` is to get all data normalized (db-like) in the data slice and ui concerns in the ui slice. The reconstruction of consumable data for the components is made in the selectors layer. Notice that I am *not* using `Immutable.js` for data safe keeping, as the library is very heavy weighted and immutability is enforced by developer.
* `Reselect`: I use reselect library for selector memoization and composition as it is very light weighted and provides nice utility methods
* `Redux-Saga`: I use this library for managing effects such as API calls. It provides a wide range of utility methods for async operations and integrates smoothly with redux by listening to the actions emmitted.
* `Styled-components`: I use this *CSS in JS* library for all styling as it allows the finest granularity on styling and preserves well-known css-like language. I have used a bunch of ready-made material-ui components (Spinner, Buttons,...) as I didn't have the time to create my own library as I would normally do in a larger project. I write *2 levels of freedom* components. By passing the `className` prop to the styled component, the parent container can directly restyle the child for a more specific use.
* `Webpack`: well, very few to say here, as I left all the heavy lifting to `react-scripts`.

## wubba lubb dub dub
I used the [Rick and Morty API](https://rickandmortyapi.com/) as I really enjoy the show and it was well documented.

## Boilerplate
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). I first used one of my personal boilerplate that included a lot more features (intl, theming, dlls, code generators,...) but since the exercise only mentionned create-react-app boilerplate, I switched back to it. I did not eject the project to keep things simple and clear, but I would definitely do it on a larger code base (I missed my webpack aliases so much :-) ).  

## Structure
* I rather use colocated folder structure as it offers a clean way to move components through the project with very few reafactoring. It may appear a bit messy at first glance (all those `components` folders), but once you're used to it, it's very simple to navigate in. 
* All shared components are in the `toolbox` folder. 
* The component code is always in the `index.js` and the styles in the the `styles.js` of the same folder. 
* All app bootstraping, middlewares and enhancers is made at the root of `src` folder in the dedicated files (`configureStore` for redux bootstrap and saga middleware, `reducers` for reducers composition, `sagas` for saga composition)
* `global-styles` provides a global css base for the app.

## Improvements
* The detail page is really basic and present few infos, I could have added more.
* Styling is not responsive (well not in all situations), I could have add some media queries to make it loook nicer on smaller screens.
* I implemented paging but lacked some time to act on the page size (would be very quick though).
* I did not use a graphql API since I am not yet very used to client libraries such as Relay or Apollo. I have made a series of tutorials, but not a *real-life* project. I am yet very interested in such technologies...
* I did not take time for jest tests nor test driven develoment. I focused more on the features themselves...
* I did not use Typescript as requested, but I have a strong experience with it as I developped quite a few apps with Angular 2 on my previous job.
* I did not use esLint as I am using Webstorm IDE formatting tools and prettier that does a great job on code formatting with a lot less boilerplate code than esLint.
* One big happy commit since I did it on a one code session, I would definitively go for *baby-steps* commits on a real-life project. 
