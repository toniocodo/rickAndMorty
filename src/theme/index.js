export default {
  colors: {
    darkBlue: '#272625',
    yellow: '#fff1b9',
    grey_1: '#5e5e5f',
    grey_2: '#3f4040',
  }
}
