export const actionSuffixes = {
  SUCCESS: '_SUCCESS',
  FAILURE: '_FAILURE',
}

export const generateAction = (type, suffix, payload, error) => ({
  type: generateActionType(type, suffix),
  payload,
  error,
})

export const generateActionType = (type, suffix) => `${type}${suffix}`

export const generateActionTypes = type => (
  Object.values(actionSuffixes).map(v => `${type}${v}`)
)
