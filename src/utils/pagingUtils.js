export const getRangeIds = (skip, take, max = Number.MAX_VALUE) => {
  const start = Math.max(0, skip)
  const length = Math.min(max, skip + take) - skip
  return Array.from(new Array(length), (_,index) => start + index + 1).join(',')
}
