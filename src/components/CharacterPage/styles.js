import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-flow: column wrap;
  align-items: center;
  padding-top: 1rem;
  flex-grow: 1;
`

export const ContentWrapper = styled.div`
  height: 100%;
`

export const ActionsWrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  padding-top: 1rem;
`
