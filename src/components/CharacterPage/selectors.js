import { createSelector } from 'reselect'
import { path, propEq, find } from 'ramda'

import { getCharacters } from '../App/selectors'

export const makeGetCharacter = (_, props) =>
  createSelector(
    [getCharacters, (_, props) => path(['match', 'params', 'id'], props)],
    (characters, id) => {
      if (!characters || !characters.length || isNaN(parseInt(id, 10))) {
        return {
          id: 0,
          name: '',
          status: '',
          species: '',
          type: '',
          gender: '',
          origin: {
            name: '',
            url: '',
          },
          location: {
            name: '',
            url: '',
          },
          image: '',
          episode: [],
          url: '',
          created: new Date().toISOString(),
        }
      }
      return find(propEq('id', parseInt(id, 10)), characters)
    },
  )
