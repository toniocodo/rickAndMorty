import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { withRouter } from 'react-router-dom'
import { createStructuredSelector } from 'reselect'
import { omit } from 'ramda'

import { removeCharacter, updateCharacter } from '../App/actions'
import { characterPropTypes } from '../App/constants'
import { makeGetCharacter } from './selectors'
import CharacterForm from './components/CharacterForm'
import CharacterView from './components/CharacterView'
import Button from '../../toolbox/Button'
import { ActionsWrapper, ContentWrapper, Wrapper } from './styles'

const MODES = {
  EDIT: 'edit',
  VIEW: 'view',
}

class CharacterPage extends Component {
  static propTypes = {
    character: characterPropTypes,
  }

  constructor(props) {
    super(props)
    this.state = {
      mode: MODES.VIEW,
      name: props.character.name,
      status: props.character.status,
      species: props.character.species,
      gender: props.character.gender,
    }
  }

  componentDidMount() {
    const editRegEx = new RegExp(/.*edit$/)
    this.setState({
      mode: editRegEx.exec(this.props.location.search)
        ? MODES.EDIT
        : MODES.VIEW,
    })
  }

  handleBack = () => {
    this.props.history.push('/')
  }

  handleChange = id => e => {
    this.setState({
      [id]: e.target.value,
    })
  }

  handleSave = () => {
    this.props.update(
      Object.assign({}, this.props.character, omit(['mode'], this.state)),
    )
    this.props.history.push('/')
  }

  render() {
    const { mode, name, status, species, gender } = this.state
    const { character } = this.props
    return (
      <Wrapper>
        <ContentWrapper>
          {mode === MODES.VIEW && <CharacterView character={character} />}
          {mode === MODES.EDIT && (
            <CharacterForm
              name={name}
              status={status}
              species={species}
              gender={gender}
              onChange={this.handleChange}
              onSave={this.handleSave}
            />
          )}
        </ContentWrapper>
        <ActionsWrapper>
          <Button onClick={this.handleBack}>Back</Button>
          {mode === MODES.EDIT && (
            <Button onClick={this.handleSave}>Save</Button>
          )}
        </ActionsWrapper>
      </Wrapper>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  character: makeGetCharacter(),
})

const mapDispatchToProps = dispatch => ({
  update: character => dispatch(updateCharacter(character)),
  remove: character => dispatch(removeCharacter(character.id)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)

export default compose(
  withRouter,
  withConnect,
)(CharacterPage)
