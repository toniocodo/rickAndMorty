import React from 'react'
import Proptypes from 'prop-types'

import { characterPropTypes } from '../../../App/constants'
import { Img, CharacterInfos, Row, Label, Value, Wrapper } from './styles'

const fieldsToShow = [
  'name',
  'gender',
  'status',
  'species',
]

const CharacterView = ({ character, className}) => (
  <Wrapper className={className}>
    <Img src={character.image} />
    <CharacterInfos>
      {Object.keys(character).filter(k => fieldsToShow.includes(k)).map(k => (
        <Row key={k}>
          <Label>{k}</Label>
          <Value>{character[k]}</Value>
        </Row>
      ))}
    </CharacterInfos>
  </Wrapper>
)

CharacterView.propTypes = {
  character: characterPropTypes,
  className: Proptypes.string,
}

export default CharacterView
