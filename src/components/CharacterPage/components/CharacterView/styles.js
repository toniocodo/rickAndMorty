import styled from 'styled-components'

export const Wrapper = styled.section`
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
  align-items: center;
`

export const Img = styled.img`
  height: 16rem;
  width: 16rem;
  border-radius: 50%;
`

export const CharacterInfos = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-start;
  align-items: flex-start;
  padding-left: 1rem;
  flex-grow: 1;
`

export const Row = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: baseline;
  width: 100%;
`

export const Label = styled.span`
  font-size: 1.3rem;
  font-weight: bold;
  text-transform: capitalize;
  min-width: 10vw;
  
  &:after {
    content: ":";
    padding-left: 0.3rem;
  }
`

export const Value = styled.span`
  font-size: 1rem;
`
