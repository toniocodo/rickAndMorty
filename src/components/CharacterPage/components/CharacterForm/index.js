import React from 'react'
import PropTypes from 'prop-types'

import FormInput from '../../../../toolbox/Input'

const CharacterForm = ({ name, status, species, gender, onChange, onSave }) => {
  return (
    <form onSubmit={onSave}>
      <FormInput
        id="name"
        label="Name"
        value={name}
        onChange={onChange('name')}
        margin="normal"
      />
      <FormInput
        id="status"
        label="Status"
        value={status}
        onChange={onChange('status')}
        margin="normal"
      />
      <FormInput
        id="species"
        label="Species"
        value={species}
        onChange={onChange('species')}
        margin="normal"
      />
      <FormInput
        id="gender"
        label="Gender"
        value={gender}
        onChange={onChange('gender')}
        margin="normal"
      />
    </form>
  )
}

CharacterForm.propTypes = {
  name: PropTypes.string,
  status: PropTypes.string,
  species: PropTypes.string,
  gender: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
}

export default CharacterForm
