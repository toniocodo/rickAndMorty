import { actionSuffixes, generateActionType } from '../../utils/reduxUtils'

import { FETCH_CHARACTERS, LOAD_CHARACTERS } from '../App/constants'
import { CHANGE_PAGE, CHANGE_PAGE_SIZE } from './constants'

const initialState = {
  info: null,
  isLoading: false,
  error: null,
  take: 20,
  skip: 0,
}

const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_CHARACTERS:
    case FETCH_CHARACTERS:
      return {
        ...state,
        isLoading: true,
      }
    case generateActionType(LOAD_CHARACTERS, actionSuffixes.SUCCESS):
      return {
        ...state,
        info: action.payload.info,
        isLoading: false,
        error: null,
      }
    case generateActionType(FETCH_CHARACTERS, actionSuffixes.SUCCESS):
      return {
        ...state,
        isLoading: false,
        error: null,
      }
    case generateActionType(LOAD_CHARACTERS, actionSuffixes.FAILURE):
    case generateActionType(FETCH_CHARACTERS, actionSuffixes.FAILURE):
      return {
        ...state,
        isLoading: false,
        error: action.error,
      }
    case CHANGE_PAGE:
      return {
        ...state,
        isLoading: true,
        skip: action.payload.skip,
      }
    case CHANGE_PAGE_SIZE:
      return {
        ...state,
        isLoading: true,
        take: action.payload.take,
      }
    default:
      return state
  }
}

export default homeReducer
