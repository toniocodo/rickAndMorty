import PropTypes from 'prop-types'

export const CHANGE_PAGE = 'Home/CHANGE_PAGE'
export const CHANGE_PAGE_SIZE = 'Home/CHANGE_PAGE_SIZE'

export const infoPropTypes = PropTypes.shape({
  count: PropTypes.number.isRequired,
  pages: PropTypes.number.isRequired,
  next: PropTypes.string,
  prev: PropTypes.string,
})
