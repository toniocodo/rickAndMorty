import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'redux'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import Spinner from '../../toolbox/Spinner'
import {
  loadCharacters,
  removeCharacter,
} from '../App/actions'
import { changePage } from './actions'
import { infoPropTypes } from './constants'
import { getInfo, getIsLoading, getSkip, getTake } from './selectors'
import {
  ArrowBack,
  ArrowForward,
  ArrowWrapper,
  CharacterList,
  SpinnerWrapper,
  Wrapper,
} from './styles'

class Home extends Component {
  static propTypes = {
    info: infoPropTypes,
    skip: PropTypes.number,
    take: PropTypes.number,
    isLoading: PropTypes.bool.isRequired,
    history: PropTypes.any,
    load: PropTypes.func.isRequired,
    remove: PropTypes.func.isRequired,
    changePage: PropTypes.func.isRequired,
    className: PropTypes.string,
  }

  componentDidMount() {
    if (this.props.info) {
      this.props.changePage({
        skip: this.props.skip,
        take: this.props.take,
        info: this.props.info,
      })
    } else {
      this.props.load()
    }
  }

  handleFetchNext = (skip, take, info) => () => {
    this.props.changePage({
      skip: Math.min(skip + take, info.count),
      take,
      info,
    })
  }

  handleFetchPrev = (skip, take, info) => () => {
    this.props.changePage({
      skip: Math.max(0, skip - take),
      take,
      info,
    })
  }

  handleView = id => () => {
    this.props.history.push(`/characterPage/${id}`)
  }

  handleEdit = id => () => {
    this.props.history.push(`/characterPage/${id}?edit`)
  }

  render() {
    const { info, isLoading, skip, take, remove, className } = this.props
    const canPrev = skip !== 0
    const canNext = info && skip + take < info.count
    return isLoading || !info ? (
      <SpinnerWrapper>
        <Spinner />
      </SpinnerWrapper>
    ) : (
      <Wrapper className={className}>
        <ArrowWrapper>
          {canPrev && (
            <ArrowBack onClick={this.handleFetchPrev(skip, take, info)} />
          )}
        </ArrowWrapper>
        <CharacterList
          onView={this.handleView}
          onEdit={this.handleEdit}
          onRemove={remove}
        />
        <ArrowWrapper>
          {canNext && (
            <ArrowForward onClick={this.handleFetchNext(skip, take, info)} />
          )}
        </ArrowWrapper>
      </Wrapper>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  info: getInfo,
  take: getTake,
  skip: getSkip,
  isLoading: getIsLoading,
})

const mapDispatchToProps = dispatch => ({
  load: () => dispatch(loadCharacters()),
  remove: character => () => dispatch(removeCharacter(character)),
  changePage: values => dispatch(changePage(values)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)

export default compose(
  withRouter,
  withConnect,
)(Home)
