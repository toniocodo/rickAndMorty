import { CHANGE_PAGE, CHANGE_PAGE_SIZE } from './constants'

export const changePage = payload => ({
  type: CHANGE_PAGE,
  payload,
})

export const changePageSize = payload => ({
  type: CHANGE_PAGE_SIZE,
  payload,
})
