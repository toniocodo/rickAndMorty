import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import { compose } from 'redux'

import { characterPropTypes } from '../../../App/constants'
import { Character, Wrapper } from './styles'
import { makeGetCharacters } from '../../selectors'

const CharacterList = ({ characters, onView, onEdit, onRemove, className }) => (
  <Wrapper className={className}>
    {characters.map(c => (
      <Character
        key={c.id}
        character={c}
        onView={onView(c.id)}
        onEdit={onEdit(c.id)}
        onRemove={onRemove(c)}
      />
    ))}
  </Wrapper>
)

CharacterList.propTypes = {
  characters: PropTypes.arrayOf(characterPropTypes),
  onView: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  className: PropTypes.string,
}

const mapStateToProps = createStructuredSelector({
  characters: makeGetCharacters(),
})

const withConnect = connect(mapStateToProps)

export default compose(withConnect)(CharacterList)
