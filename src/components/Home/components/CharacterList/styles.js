import styled from 'styled-components'

import OriginalCharacter from '../Character'

export const Wrapper = styled.article`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  align-items: center;
  
  > * {
    margin: 0.5rem;
  }
`

export const Character = styled(OriginalCharacter)`
  width: 20vw;
  height: 10vh;
  padding: 0.5rem;
`
