import React from 'react'
import PropTypes from 'prop-types'
import Tooltip from '@material-ui/core/Tooltip'

import { characterPropTypes } from '../../../App/constants'
import {
  CharacterInfos,
  Img,
  Actions,
  Wrapper,
  Edit,
  Remove,
  View,
} from './styles'

const Character = ({ character, onView, onEdit, onRemove, className }) => (
  <Wrapper className={className}>
    <Img src={character.image} />
    <CharacterInfos>{character.name}</CharacterInfos>
    <Actions>
      {onView && (
        <Tooltip title="View details" placement="right">
          <View onClick={onView} />
        </Tooltip>
      )}
      {onEdit && (
        <Tooltip title="Edit details" placement="right">
          <Edit onClick={onEdit} />
        </Tooltip>
      )}
      {onRemove && (
        <Tooltip title="Remove character" placement="right">
          <Remove onClick={onRemove} />
        </Tooltip>
      )}
    </Actions>
  </Wrapper>
)

Character.propTypes = {
  character: characterPropTypes.isRequired,
  onView: PropTypes.func,
  onEdit: PropTypes.func,
  onRemove: PropTypes.func,
  className: PropTypes.string,
}

export default Character
