import styled from 'styled-components'

import theme from '../../../../theme'
import { withStyles } from '@material-ui/core'
import OriginalView from '@material-ui/icons/RemoveRedEye'
import OriginalEdit from '@material-ui/icons/Edit'
import OriginalRemove from '@material-ui/icons/RemoveCircle'

export const Wrapper = styled.article`
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
  align-items: center;
  border: 1px solid ${theme.colors.grey_2};
  background-color: ${theme.colors.grey_2};
  position: relative;

  &:hover {
    background-color: ${theme.colors.grey_1};
    border: 1px solid ${theme.colors.yellow};
  }
`

export const Img = styled.img`
  height: 6rem;
  width: 6rem;
  border-radius: 50%;
`

export const CharacterInfos = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-start;
  align-items: flex-start;
  padding-left: 1rem;
`

export const Actions = styled.div`
  position: absolute;
  right: 0.2rem;
  top: 0.2rem;
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-start;
  align-items: flex-start;
`

const iconStyles = {
  root: {
    color: theme.colors.yellow,
    fontSize: 20,
    paddingTop: '0.5rem',
    cursor: 'pointer',
    '&:hover': {
      color: theme.colors.grey_2
    }
  },
}

export const View = withStyles(iconStyles)(OriginalView)
export const Remove = withStyles(iconStyles)(OriginalRemove)
export const Edit = withStyles(iconStyles)(OriginalEdit)
