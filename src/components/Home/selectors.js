import { createSelector } from 'reselect'
import { take } from 'ramda'

import { getCharacters, getHome } from '../App/selectors'

export const getInfo = state => getHome(state).info
export const getIsLoading = state => getHome(state).isLoading
export const getError = state => getHome(state).error
export const getTake = state => getHome(state).take
export const getSkip = state => getHome(state).skip

export const makeGetCharacters = () =>
  createSelector([getCharacters, getTake], (characters, takeCount) => {
    if (!characters || !characters.length || takeCount === 0) {
      return []
    }
    return take(takeCount, characters)
  })
