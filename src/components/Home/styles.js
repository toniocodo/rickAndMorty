import styled from "styled-components"
import OriginalArrowBack from "@material-ui/icons/ArrowBack"
import OriginalArrowForward from "@material-ui/icons/ArrowForward"
import { withStyles } from "@material-ui/core"

import theme from '../../theme'
import OriginalCharacterList from "./components/CharacterList"

export const SpinnerWrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  min-height: 50vh;
`

export const Wrapper = styled.section`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-evenly;
  align-items: center;
  padding-top: 1rem;
`

export const CharacterList = styled(OriginalCharacterList)`
  width: 100%;
`

export const ArrowWrapper = styled.div`
  min-width: 3vw;
  height: 100%;
`

const iconStyles = {
  root: {
    color: theme.colors.yellow,
    fontSize: 50,
    cursor: 'pointer',
    '&:hover': {
      color: theme.colors.grey_2
    },
  },
}

export const ArrowForward = withStyles(iconStyles)(OriginalArrowForward)
export const ArrowBack = withStyles(iconStyles)(OriginalArrowBack)
