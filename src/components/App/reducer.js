import { combineReducers } from 'redux'

import { actionSuffixes, generateActionType } from '../../utils/reduxUtils'
import homeReducer from '../Home/reducer'

import { LOAD_CHARACTERS, CLOSE_SNACK, FETCH_CHARACTERS, OPEN_SNACK } from './constants'

const dataInitialState = {
  characters: [],
}

const dataReducer = (state = dataInitialState, action) => {
  switch (action.type) {
    case generateActionType(LOAD_CHARACTERS, actionSuffixes.SUCCESS):
      return {
        ...state,
        characters: action.payload.results,
      }
    case generateActionType(FETCH_CHARACTERS, actionSuffixes.SUCCESS):
      return {
        ...state,
        characters: action.payload,
      }
    default:
      return state
  }
}

const appInitialState = {
  snackOpen: false,
  snackMessage: '',
}

const appReducer = (state = appInitialState, action) => {
  switch (action.type) {
    case OPEN_SNACK:
      return {
        ...state,
        snackOpen: true,
        snackMessage: action.payload
      }
    case CLOSE_SNACK:
      return {
        ...state,
        snackOpen: false,
      }
    default:
      return state
  }
}

export default combineReducers({
  data: dataReducer,
  ui: combineReducers({
    app: appReducer,
    home: homeReducer,
  }),
})
