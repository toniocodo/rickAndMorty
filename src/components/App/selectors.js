export const getGlobal = state => state.global
export const getData = state => getGlobal(state).data
export const getUi = state => getGlobal(state).ui
export const getApp = state => getUi(state).app
export const getHome = state => getUi(state).home
export const getCharacterPage = state => getUi(state).characterPage

export const getSnackOpen = state => getApp(state).snackOpen
export const getSnackMessage = state => getApp(state).snackMessage
export const getCharacters = state => getData(state).characters
