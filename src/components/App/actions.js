import {
  CLOSE_SNACK,
  LOAD_CHARACTERS,
  OPEN_SNACK,
  REMOVE_CHARACTER,
  UPDATE_CHARACTER,
} from './constants'

export const loadCharacters = () => ({
  type: LOAD_CHARACTERS,
})

export const updateCharacter = payload => ({
  type: UPDATE_CHARACTER,
  payload,
})

export const removeCharacter = payload => ({
  type: REMOVE_CHARACTER,
  payload,
})

export const openSnack = payload => ({
  type: OPEN_SNACK,
  payload,
})

export const closeSnack = () => ({
  type: CLOSE_SNACK,
})
