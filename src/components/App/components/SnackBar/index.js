import React from 'react'
import PropTypes from 'prop-types'
import OriginalSnackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'

const Snackbar = ({ message, isOpen, onClose }) => (
  <OriginalSnackbar
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'left',
    }}
    open={isOpen}
    autoHideDuration={1500}
    onClose={onClose}
    ContentProps={{
      'aria-describedby': 'message-id',
    }}
    message={<span id="message-id">{message}</span>}
    action={[
      <IconButton
        key="close"
        aria-label="Close"
        color="inherit"
        onClick={onClose}
      >
        <CloseIcon />
      </IconButton>,
    ]}
  />
)

Snackbar.propTypes = {
  message: PropTypes.string,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
}

Snackbar.defaultProps = {
  message: '',
  isOpen: false,
}

export default Snackbar
