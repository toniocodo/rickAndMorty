import React from 'react'
import Proptypes from 'prop-types'

import banner from './banner.png'
import { Img, Label, Wrapper } from './styles'

const Banner = ({ label, className }) => (
  <Wrapper className={className}>
    <Img src={banner} />
    {label && <Label>{label}</Label>}
  </Wrapper>
)

Banner.propTypes = {
  label: Proptypes.string,
  className: Proptypes.string,
}

export default Banner
