import styled from 'styled-components'

export const Wrapper = styled.article`
  position: relative;
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
  align-items: flex-start;
`

export const Img = styled.img`
  width: 100%;  
`

export const Label = styled.span`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-size: 4rem;
  font-weight: bold;
  text-align: center;
  width: 100%;
  text-shadow: 3px 3px 0 #2c2e38, 5px 5px 0 #5c5f72;
`
