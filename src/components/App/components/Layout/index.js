import React from 'react'
import Proptypes from 'prop-types'

import { Banner, Content, Wrapper } from './styles'

const Layout = ({ children, snackBar, className}) => (
  <Wrapper className={className}>
    <Banner label="Rick and Morty" />
    <Content>{children}</Content>
    {snackBar}
  </Wrapper>
)

Layout.propTypes = {
  children: Proptypes.element,
  className: Proptypes.string,
  snackBar: Proptypes.element,
}

export default Layout
