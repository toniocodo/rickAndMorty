import styled from 'styled-components'

import theme from '../../../../theme'
import OriginalBanner from './components/Banner'

export const Wrapper = styled.section`
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-start;
  align-items: flex-start;
  background-color: ${theme.colors.darkBlue};
  color: ${theme.colors.yellow};
`

export const Banner = styled(OriginalBanner)`
  width: 100%;
`

export const Content = styled.section`
  width: 100%;
  flex-grow: 1;
`
