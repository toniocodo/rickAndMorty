import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'redux'
import { Route, Switch, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import Home from '../Home'
import CharacterPage from '../CharacterPage'
import Layout from './components/Layout'
import Snackbar from './components/SnackBar'
import { getSnackMessage, getSnackOpen } from './selectors'
import { closeSnack } from './actions'

class App extends Component {
  static propTypes = {
    snackOpen: PropTypes.bool.isRequired,
    snackMessage: PropTypes.string.isRequired,
    onCloseSnack: PropTypes.func.isRequired,
  }

  render() {
    const { snackOpen, snackMessage, onCloseSnack } = this.props
    return (
      <Layout
        snackBar={
          <Snackbar
            isOpen={snackOpen}
            message={snackMessage}
            onClose={onCloseSnack}
          />
        }
      >
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/characterPage/:id" component={CharacterPage} />
        </Switch>
      </Layout>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  snackOpen: getSnackOpen,
  snackMessage: getSnackMessage,
})

const mapDispatchToProps = dispatch => ({
  onCloseSnack: () => dispatch(closeSnack()),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
)

export default compose(
  withRouter,
  withConnect,
)(App)
