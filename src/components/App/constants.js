import PropTypes from 'prop-types'

export const LOAD_CHARACTERS = 'App/LOAD_CHARACTERS'
export const FETCH_CHARACTERS = 'App/FETCH_CHARACTERS'
export const UPDATE_CHARACTER = 'App/UPDATE_CHARACTER'
export const REMOVE_CHARACTER = 'App/REMOVE_CHARACTER'

export const OPEN_SNACK = 'App/OPEN_SNACK'
export const CLOSE_SNACK = 'App/CLOSE_SNACK'

export const locationPropTypes = PropTypes.shape({
  name: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
})

export const characterPropTypes = PropTypes.shape({
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  species: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  gender: PropTypes.string.isRequired,
  origin: locationPropTypes,
  location: locationPropTypes,
  image: PropTypes.string.isRequired,
  episode: PropTypes.arrayOf(PropTypes.string),
  url: PropTypes.string.isRequired,
  created: PropTypes.string.isRequired,
})
