import axios from 'axios'
import { call, put, takeLatest } from 'redux-saga/effects'

import { actionSuffixes, generateAction } from '../../utils/reduxUtils'
import { openSnack } from './actions'
import {
  FETCH_CHARACTERS,
  LOAD_CHARACTERS,
  REMOVE_CHARACTER,
  UPDATE_CHARACTER,
} from './constants'
import { CHANGE_PAGE } from '../Home/constants'
import { getRangeIds } from '../../utils/pagingUtils'

const loadCharacters = () => {
  return axios
    .get('https://rickandmortyapi.com/api/character/')
    .then(res => ({ data: res.data }))
    .catch(err => ({ error: err }))
}

const fetchCharacters = ids => {
  return axios
    .get(`https://rickandmortyapi.com/api/character/${ids}`)
    .then(res => ({ data: res.data }))
    .catch(err => ({ error: err }))
}

function* loadWorker(action) {
  try {
    const results = yield call(loadCharacters)
    if (results.error) {
      yield put(
        generateAction(
          action.type,
          actionSuffixes.FAILURE,
          null,
          results.error,
        ),
      )
    }
    if (results.data) {
      yield put(
        generateAction(action.type, actionSuffixes.SUCCESS, results.data, null),
      )
    }
  } catch (e) {
    yield put(
      generateAction(
        action.type,
        actionSuffixes.FAILURE,
        null,
        'Error loading characters',
      ),
    )
    yield put(openSnack('Error loading characters'))
  }
  return undefined
}

function* fetchWorker(action) {
  try {
    let ids = ''
    if (action.payload.ids) {
      ids = action.payload.ids.join(',')
    } else {
      ids = getRangeIds(
        action.payload.skip,
        action.payload.take,
        action.payload.info.count,
      )
    }
    const results = yield call(fetchCharacters, ids)
    if (results.error) {
      yield put(
        generateAction(
          FETCH_CHARACTERS,
          actionSuffixes.FAILURE,
          null,
          results.error,
        ),
      )
    }
    if (results.data) {
      yield put(
        generateAction(
          FETCH_CHARACTERS,
          actionSuffixes.SUCCESS,
          results.data,
          null,
        ),
      )
    }
  } catch (e) {
    yield put(
      generateAction(
        FETCH_CHARACTERS,
        actionSuffixes.FAILURE,
        null,
        'Error fetching characters',
      ),
    )
    yield put(openSnack('Error fetching characters'))
  }
  return undefined
}

function* updateWorker(action) {
  /*
    do api save here
   */
  yield put(openSnack(`Character ${action.payload.name} updated`))
  return undefined
}

function* removeWorker(action) {
  /*
    do api remove here
   */
  yield put(openSnack(`Character ${action.payload.name} removed`))

  return undefined
}

function* watcher() {
  yield takeLatest(LOAD_CHARACTERS, loadWorker)
  yield takeLatest([CHANGE_PAGE, FETCH_CHARACTERS], fetchWorker)
  yield takeLatest(UPDATE_CHARACTER, updateWorker)
  yield takeLatest(REMOVE_CHARACTER, removeWorker)
}

export default [watcher]
