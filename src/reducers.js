import { combineReducers } from 'redux'
import { LOCATION_CHANGE } from 'react-router-redux'

import appReducer from './components/App/reducer'

const routeInitialState = {
  location: null,
}

const routeReducer = (state = routeInitialState, action) => {
  switch (action.type) {
    case LOCATION_CHANGE:
      return Object.assign({}, state, {
        location: action.payload,
      })
    default:
      return state
  }
}

const createReducer = () => (
  combineReducers({
    route: routeReducer,
    global: appReducer,
  })
)

export default createReducer
