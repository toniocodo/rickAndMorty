import OriginalButton from '@material-ui/core/Button'
import { withStyles } from "@material-ui/core"

import theme from '../../theme'

const buttonStyle = {
  root: {
    margin: '0.3rem',
    color: theme.colors.yellow,
    borderColor: theme.colors.yellow,
  }
}

export const Button = withStyles(buttonStyle)(OriginalButton)
