import React from 'react'
import Proptypes from 'prop-types'

import { Button as StyledButton } from './styles'

const Button = ({ children, onClick }) => (
  <StyledButton variant="outlined" onClick={onClick}>
    {children}
  </StyledButton>
)

Button.propTypes = {
  children: Proptypes.string.isRequired,
  onClick: Proptypes.func.isRequired,
}

export default Button
