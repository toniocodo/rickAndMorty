import styled from 'styled-components'

import theme from '../../theme'

export const Wrapper = styled.section`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: baseline;  
  color: ${theme.colors.yellow};
  padding: 0.3rem;
`

export const Label = styled.span`
  min-width: 5vw;
  font-size: 1.3rem;
  font-weight: bold;
  padding-right: 1rem;
  
  &:after {
    content: ":";
    padding-left: 0.2rem;
  }
`

export const Input = styled.input`
  min-width: 10vw;
  border: none;
`
