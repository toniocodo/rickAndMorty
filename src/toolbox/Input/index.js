import React from 'react'
import Proptypes from 'prop-types'

import { Label, Input as StyledInput, Wrapper } from './styles'

const Input = ({ id, label, value, onChange, className}) => (
  <Wrapper className={className}>
    <Label>{label}</Label>
    <StyledInput
      id={id}
      value={value}
      onChange={onChange}
    />
  </Wrapper>
)

Input.propTypes = {
  id: Proptypes.string.isRequired,
  label: Proptypes.string.isRequired,
  value: Proptypes.string,
  onChange: Proptypes.func.isRequired,
  className: Proptypes.string,
}

export default Input
