import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import yellow from '@material-ui/core/colors/yellow'

const Spinner = () => (
  <CircularProgress
    style={{ color: yellow[300] }}
    thickness={4}
  />
)

export default Spinner
