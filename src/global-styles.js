import { injectGlobal } from 'styled-components'

injectGlobal`
  html,
  body {
    margin: 0;
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #root {
    min-height: 100%;
    min-width: 100%;
    display: flex;
  }
`
